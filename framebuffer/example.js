// https://www.npmjs.com/package/canvas
// https://github.com/Automattic/node-canvas/wiki/Using-with-framebuffer-devices

const fs = require('fs')
const { createCanvas, loadImage } = require('canvas')
const canvas = createCanvas(1920, 1080)
const ctx = canvas.getContext('2d')

// Write "Awesome!"
ctx.font = '300px Impact'
// ctx.rotate(0.1)
ctx.fillText('Awesome!', 50, 200)

// Draw line under text
var text = ctx.measureText('Awesome!')
ctx.strokeStyle = 'rgba(0,0,0,0.5)'
// ctx.beginPath()
// ctx.lineTo(50, 102)
// ctx.lineTo(50 + text.width, 102)
// ctx.stroke()

// Draw cat with lime helmet
loadImage('out.png').then((image) => {
    ctx.drawImage(image, 50, 0, 70, 70)

    fs.writeFileSync("test.html", '<img src="' + canvas.toDataURL() + '" />')
})