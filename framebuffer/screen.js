// https://www.npmjs.com/package/canvas
// https://github.com/Automattic/node-canvas/wiki/Using-with-framebuffer-devices

const fs = require('fs')
const { registerFont, createCanvas, loadImage } = require('canvas')
registerFont('./fonts/Roboto_Mono/static/RobotoMono-Light.ttf', { family: 'Roboto Mono' })

const canvas = createCanvas(1824,984)
const ctx = canvas.getContext('2d',{pixelFormat:"RGB32_256"})

const h = canvas.height
const w = canvas.width
const time = getTime()

ctx.fillStyle = 'rgb(0,0,0)'
ctx.fillRect(0, 0, w, h)

ctx.font = `${vh(20)}px Roboto Mono`
ctx.fillStyle = 'rgb(255,255,255)'
ctx.fillText(time, vh(5), vh(20))
var text = ctx.measureText(time)


loadImage('out.png').then((image) => {
    console.log(image.width)
let aspect = image.width / image.height

    let height = Math.max(
        image.height / (aspect * (w - (text.width + vh(5) + vh(5)))),
        h-vh(5))

    let width = height * aspect

    ctx.drawImage(image, text.width + vh(5) + vh(5), vh(2.5), width, height)

    writeToScreen()
    //fs.writeFileSync("test.html", '<img src="' + canvas.toDataURL() + '" />')
})


function writeToScreen() {
    const fb = fs.openSync("/dev/fb0", "w"); // where /dev/fb0 is the path to your fb device
    const buff = canvas.toBuffer("raw");
    // console.log(buff.byteLength);  // should be equal to width*height*2
    fs.writeSync(fb, buff, 0, buff.byteLength, 0);
}

function vh(x) {
    return Math.round(x * h / 100)
}

function getTime() {
    let d = new Date()
    let h = ((d.getHours() - 1) % 12) + 1
    let m = d.getMinutes()
    m = m < 10 ? '0' + m : "" + m
    h = h < 10 ? " " + h : "" + h
    return h + ":" + m
}
