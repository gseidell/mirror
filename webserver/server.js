const fs = require('fs')
const updateImage = require('./updateImage.js')
const screen = require("./screen.js")
const express = require('express')
const app = express()
const port = 3000

var htmlFile = fs.readFileSync('./main.html', 'UTF8')

var mode = "clockAndPic"

setInterval(() => {
    screen.draw()
}, 1000);

app.use(express.static(__dirname));

app.get('/', (req, res) => {
    res.send(htmlFile.replace(`var mode = "clockAndPic"`, `var mode = "${mode}"`))
})

app.get('/toggleMode', (req, res) => {
    mode = mode == "clockAndPic" ? "clock" : "clockAndPic"
    res.send(`${mode}`)
})

app.get('/updatePic', async (req, res) => {
    await updateImage.updatePicture()
    res.send("updated")
})

app.get('/savePic', async (req, res) => {
    fs.copyFile("out.png", "./pictures/" + Date.now() + ".png", () => { })
    res.send("saved")
})

app.get('/showPic', async (req, res) => {
    fileName = req.query["fileName"]
    fs.copyFile("./pictures/" + fileName, "out.png", () => { })
    res.send("loaded")
})

app.get('/listSaved', async (req, res) => {
    res.json(JSON.stringify(fs.readdirSync('./pictures')))
})

app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`)
})
