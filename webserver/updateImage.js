const Jimp = require('jimp');
const axios = require('axios');

// main()
async function main() {
    await choosePicture()
    return true
}

async function choosePicture() {

    let period = ["week", "month", "year", "all"][Math.floor(Math.random() * 4)]
    var getReq = await axios({
        method: "get",
        url: `https://www.reddit.com/r/Amoledbackgrounds/top/.json?t=${period}&limit=100`,
        responseType: "json"
    })
    let post = 0
    let count = 0
    do {
        post = Math.floor(Math.random() * 100)
        count++
        console.log(getReq.data.data.children[post].data.url)
        console.log(process.memoryUsage().heapUsed / 1024 / 1024)
    }
    while (!await downloadPicture(getReq.data.data.children[post].data.url))
    console.log(count)
    return getReq.data.data.children[post].data.url

}

async function downloadPicture(url) {
    return await Jimp.read("" + url)
        .then(image => {

            let crop = findCrop(image)

            if (crop.allCropped) {
                console.log("writng")
                image
                    .crop(crop.x, crop.y, crop.w, crop.h)
                    .write("out.png");
                return true
            }
            return false
        })
        .catch(err => {
            console.log(err)
        });
}

function findCrop(image) {

    let blackThreshold = 60
    let width = image.bitmap.width
    let height = image.bitmap.height

    let top = 0
    let flag = false
    for (top = 0; top != height && !flag; top++) {
        for (let r = 0; r != width; r++) {
            let p = Jimp.intToRGBA(image.getPixelColor(r, top))
            if (p.r + p.g + p.b > blackThreshold)
                flag = true
        }
    }
    top--

    let bot = 0
    flag = false
    for (bot = height - 1; bot != -1 && !flag; bot--) {
        for (let r = 0; r != width; r++) {
            let p = Jimp.intToRGBA(image.getPixelColor(r, bot))
            if (p.r + p.g + p.b > blackThreshold)
                flag = true
        }
    }
    bot++

    let left = 0
    flag = false
    for (left = 0; left != width && !flag; left++) {
        for (let c = 0; c != height; c++) {
            let p = Jimp.intToRGBA(image.getPixelColor(left, c))
            if (p.r + p.g + p.b > blackThreshold)
                flag = true
        }
    }
    left--

    let right = 0
    flag = false
    for (right = width - 1; right != -1 && !flag; right--) {
        for (let c = 0; c != height; c++) {
            let p = Jimp.intToRGBA(image.getPixelColor(right, c))
            if (p.r + p.g + p.b > blackThreshold)
                flag = true
        }
    }
    right++

    console.log({ width, height })
    console.log({ top, bot, left, right })

    let allCropped =
        top != 0 &&
        left != 0 &&
        bot != height &&
        right != width
        &&
        top < bot &&
        left < right

    console.log(allCropped)

    return ({
        x: left,
        y: top,
        w: right - left,
        h: bot - top,
        allCropped
    })
}

module.exports = { updatePicture: main }